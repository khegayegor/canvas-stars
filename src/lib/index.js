import { drawAllStars, onStarsCanvasClick } from "./utils.js";
import starsList from './data/starsList.js';

const starsCanvas = document.getElementById('stars');
const colorCanvas = document.getElementById('color');

// Рисуем все звезды
drawAllStars(starsCanvas, starsList);

// Вешаем обработчик клика по канвасу со звездами
starsCanvas.addEventListener('click', function (event) {
	onStarsCanvasClick(event, colorCanvas);
});