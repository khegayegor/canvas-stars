export default [
	{
		center: [300, 147],
		vertexCount: 5,
		size: [90, 35],
		color: 'red'
	},
	{
		center: [150, 250],
		vertexCount: 5,
		size: [90, 35],
		color: 'blue'
	},
	{
		center: [450, 250],
		vertexCount: 5,
		size: [90, 35],
		color: 'green'
	},
	{
		center: [208, 420],
		vertexCount: 5,
		size: [90, 35],
		color: 'yellow'
	},
	{
		center: [392, 420],
		vertexCount: 5,
		size: [90, 35],
		color: 'black'
	},
];