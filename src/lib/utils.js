/**
 * Функция для рисования звезды по переданным параметрам
 *
 * @param context - контекст рисования
 * @param centerX - центр звезды по оси X
 * @param centerY - центр звезды по оси Y
 * @param vertexCount - количество вершин
 * @param outerRadius - внешний радиус
 * @param innerRadius - внутренний радиус
 * @param fillColor - цвет заливки
 */
function drawStar(context, centerX, centerY, vertexCount, outerRadius, innerRadius, fillColor) {
	let rotation = Math.PI /2 * 3;
	let x = centerX;
	let y = centerY;
	let step = Math.PI / vertexCount;

	context.beginPath();
	context.moveTo(centerX, centerY + outerRadius);

	for (let i = 0; i < vertexCount; i++) {
		x = centerX + Math.cos(rotation) * outerRadius;
		y = centerY + Math.sin(rotation) * outerRadius;
		context.lineTo(x,y);
		rotation += step
		x = centerX + Math.cos(rotation) * innerRadius;
		y = centerY + Math.sin(rotation) * innerRadius;
		context.lineTo(x,y);
		rotation += step;
	}

	context.lineTo(centerX, centerY - outerRadius);
	context.closePath();
	context.fillStyle = fillColor;
	context.fill();
}

/**
 *  Функция для получения цвета пикселя, в переданном контексте
 *
 * @param event
 * @param context
 * @return {string}
 */
function getColor(event, context) {
	const target = event.target;
	const x = event.pageX - target.offsetLeft;
	const y = event.pageY - target.offsetTop;
	const [ red, green, blue, alpha_int ] = context.getImageData(x, y, 1, 1).data;

	if (red === green && green === blue && blue === alpha_int && alpha_int === 0) {
		return 'white';
	}

	const alpha = parseFloat((alpha_int / 255).toFixed(2));

	return `rgba(${red},${green},${blue},${alpha})`;
}

/**
 * Функция изменения цвета канваса
 *
 * @param canvas
 * @param fillColor
 */
function changeFill(canvas, fillColor) {
	const context = canvas.getContext('2d');
	const canvasWidth = canvas.width;
	const canvasHeight = canvas.height;

	context.fillStyle = fillColor;
	context.clearRect(0, 0, canvasWidth, canvasHeight);
	context.fillRect(0, 0, canvasWidth, canvasHeight);
}

/**
 * Функция для отрисовки из списка звезд в переданном канвасе
 *
 * @param canvas
 * @param list
 */
function drawAllStars(canvas, list) {
	const context = canvas.getContext('2d');

	for (let item of list) {
		const { vertexCount, color, center, size } = item;
		const [ centerX, centerY ] = center;
		const [ outerRadius, innerRadius ] = size;

		drawStar(context, centerX, centerY, vertexCount, outerRadius, innerRadius, color);
	}
}

/**
 * Обработчик клика по канвасу
 *
 * @param event
 * @param colorCanvas
 */
function onStarsCanvasClick(event, colorCanvas) {
	const starsCanvas = event.target;
	const starsContext = starsCanvas.getContext('2d');
	const color = getColor(event, starsContext);

	changeFill(colorCanvas, color);
}

export { drawAllStars, onStarsCanvasClick };